import torch
from torch import Tensor as T
from torch import nn
from torch import FloatTensor
from typing import List, Tuple, Dict, Iterator
import copy
import os
import logging
from dpr.utils.model_utils import setup_for_distributed_mode, get_model_obj, load_states_from_checkpoint,move_to_device
from dpr.models import init_biencoder_components
from dpr.options import set_encoder_params_from_state
from scipy import spatial

logger = logging.getLogger(__name__)

class DenseRetriever(object):
    """
    Does passage retrieving over the provided index and question encoder
    """
    def __init__(self, args, pretrained_file:str, batch_size: int, use_gpu: bool):
        self.batch_size = batch_size        
        saved_state = load_states_from_checkpoint(args.model_file)
        set_encoder_params_from_state(saved_state.encoder_params, args)
        args.pretrained_file=pretrained_file
        
        self.device=args.device

        self.tensorizer, biencoder, _ = init_biencoder_components(args.encoder_model_type, args, inference_only=True)
        
        
        self.ctx_encoder = biencoder.ctx_model
        
        if use_gpu:
            self.ctx_encoder, _ = setup_for_distributed_mode(self.ctx_encoder, None, args.device, args.n_gpu,
                                                args.local_rank,
                                                args.fp16)
        self.ctx_encoder.eval()
        
        # load weights from the model file for ctx encoder
        ctx_model_to_load = get_model_obj(self.ctx_encoder)
        prefix_len = len('ctx_model.')
        ctx_state = {key[prefix_len:]: value for (key, value) in saved_state.model_dict.items() if
                 key.startswith('ctx_model.')}
        ctx_model_to_load.load_state_dict(ctx_state)
        
        self.question_encoder = biencoder.question_model
        
        if use_gpu:
            self.question_encoder, _ = setup_for_distributed_mode(self.question_encoder, None, args.device, args.n_gpu,args.local_rank,args.fp16)
        
            
        # load weights from the model file for question encoder
        question_model_to_load = get_model_obj(self.question_encoder)      
        prefix_len = len('question_model.')
        question_state = {key[prefix_len:]: value for (key, value) in saved_state.model_dict.items() if
                                      key.startswith('question_model.')}
        question_model_to_load.load_state_dict(question_state)
        
    def generate_question_vectors(self, questions: List[str]) -> T:
        n = len(questions)
        bsz = self.batch_size
        query_vectors = []
        
        self.question_encoder.eval()
        with torch.no_grad():
            for j, batch_start in enumerate(range(0, n, bsz)):

                batch_token_tensors = [self.tensorizer.text_to_tensor(q) for q in
                                       questions[batch_start:batch_start + bsz]]

                q_ids_batch = torch.stack(batch_token_tensors, dim=0)
                q_seg_batch = torch.zeros_like(q_ids_batch)
                q_attn_mask = self.tensorizer.get_attn_mask(q_ids_batch)
                _, out, _ = self.question_encoder(q_ids_batch, q_seg_batch, q_attn_mask)

                query_vectors.extend(out.cpu().split(1, dim=0))

                if len(query_vectors) % 100 == 0:
                    logger.info('Encoded queries %d', len(query_vectors))

        query_tensor = torch.cat(query_vectors, dim=0)

        assert query_tensor.size(0) == len(questions)
        return query_tensor
    
    def gen_ctx_vectors(self,ctx_rows, key ,key_title=None) :
        
        '''ctx_rows
        |          [{'id': 'dfdfs',
        |            'text': ' albert einstein'
        |          }]
        '''
        n = len(ctx_rows)
        bsz = self.batch_size
        total = 0
        results = []
        
        matrix=FloatTensor()
        for j, batch_start in enumerate(range(0, n, bsz)):

            batch_token_tensors = [self.tensorizer.text_to_tensor(ctx[key], title=None) for ctx in
                                   ctx_rows[batch_start:batch_start + bsz]]

            ctx_ids_batch = move_to_device(torch.stack(batch_token_tensors, dim=0),self.device)
            ctx_seg_batch = move_to_device(torch.zeros_like(ctx_ids_batch),self.device)
            ctx_attn_mask = move_to_device(self.tensorizer.get_attn_mask(ctx_ids_batch),self.device)
            with torch.no_grad():
                _, out, _ = self.ctx_encoder(ctx_ids_batch, ctx_seg_batch, ctx_attn_mask)
            out = out.cpu()
            
            matrix = torch.cat([matrix, out], dim=0)

        return matrix
    
    def compute_consine_score(self,query_text,corpus_json,key='text',key_title=None,use_ctx_encoder:bool=True): 
        """
        This function performs a cosine similarity search between query text and corpus represented by the list of dictionary output_json:
            [{"id":0,"text":"abc"},{"id":1,"text":"abd"}].
        :param query_text: query_text
        :param output_json: list of dict with the keys id [{"id":0,"text":"abc"}]
        :param key: key of dict that is used for getting corpus text.
        :use_ctx_encoder: if True: corpus_encode=ctx_encoder, else: use_ctx_encoder=question_encoder.
        :return: Returns list of dict sorted by decreasing cosine similarity scores.
        """
        def set_score(x_score):
            x,score=x_score
            x["score"]=1-score
            return x
        
        questions_tensor = self.generate_question_vectors([query_text])
        
        if use_ctx_encoder:
            doc_matrix = self.gen_ctx_vectors(corpus_json,key,key_title)    
            score_list=spatial.distance.cdist(doc_matrix, questions_tensor, 'cosine').reshape(-1)
            corpus_json = list(map(set_score, zip(corpus_json,score_list)))
#             for j,(id,vec) in enumerate(doc_vectors):
#                 corpus_json[j]["score"]=1 - spatial.distance.cosine(questions_tensor[0].data.numpy(),vec)
        
        else:
            question_list = list(map(lambda x : x[key], corpus_json))
            question_list_tensor = self.generate_question_vectors(question_list)
            for j in range(question_list_tensor.shape[0]):
                corpus_json[j]["score"]=1 - spatial.distance.cosine(questions_tensor[0], question_list_tensor[j])


        corpus_json=sorted(corpus_json, key=lambda x: x['score'], reverse=True)
        return corpus_json