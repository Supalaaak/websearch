import requests
from typing import List
from tqdm import tqdm
import sys
import importlib
import os
import numpy as np
import queue
import torch
import logging
import argparse
from dpr.options import add_encoder_params, setup_args_gpu, print_args, set_encoder_params_from_state, \
    add_tokenizer_params, add_cuda_params
from dpr.generator.transformers import DenseRetriever
from config import __PRETRAINED_MODEL_SERVER__,__PRETRAINED_MODEL_LOCATION__,__DPR_MODEL_SERVER__,__DPR_LOCATION__,__USE_GPU__
from zipfile import ZipFile,is_zipfile
import shutil
from knowledge_base.utils import encode_array

logger = logging.getLogger(__name__)

DenseEncoder=None

def http_get(url, path):
    """
    Downloads a URL to a given path on disc
    """
    if os.path.dirname(path) != '':
        os.makedirs(os.path.dirname(path), exist_ok=True)

    req = requests.get(url, stream=True)
    if req.status_code != 200:
        print("Exception when trying to download {}. Response {}".format(url, req.status_code), file=sys.stderr)
        req.raise_for_status()
        return

    download_filepath = path+"_part"
    with open(download_filepath, "wb") as file_binary:
        content_length = req.headers.get('Content-Length')
        total = int(content_length) if content_length is not None else None
        progress = tqdm(unit="B", total=total, unit_scale=True)
        for chunk in req.iter_content(chunk_size=1024):
            if chunk: # filter out keep-alive new chunks
                progress.update(len(chunk))
                file_binary.write(chunk)

    os.rename(download_filepath, path)
    progress.close()

def download_model():  
    
    if not(os.path.exists(__PRETRAINED_MODEL_LOCATION__)):
        logging.info("Downloading pretrained transformer model from {} and saving it at {}".format(__PRETRAINED_MODEL_SERVER__,__PRETRAINED_MODEL_LOCATION__))
        zip_save_path=__PRETRAINED_MODEL_LOCATION__+".zip"
        http_get(__PRETRAINED_MODEL_SERVER__,zip_save_path) 
        model_path=os.path.dirname(__PRETRAINED_MODEL_LOCATION__)
        try:
            with ZipFile(zip_save_path, 'r') as zip:
                zip.extractall(model_path)
        except Exception as e:
            shutil.rmtree(model_path)
            raise e
            
        if os.path.exists(zip_save_path):
            os.remove(zip_save_path)
            
    if not(os.path.exists(__DPR_LOCATION__)):
        logging.info("Downloading DPR model from {} and saving it at {}".format(__DPR_MODEL_SERVER__,__DPR_LOCATION__))
        http_get(__DPR_MODEL_SERVER__,__DPR_LOCATION__) 
    
def setup_args_from_checkpoint(model_name_or_path: str, use_gpu: bool):
    parser = argparse.ArgumentParser()
    add_encoder_params(parser)
    add_tokenizer_params(parser)
    parser.add_argument('--n-docs', type=int, default=200, help="Amount of top docs to return")
    parser.add_argument('--batch_size', type=int, default=2, help="Batch size for question encoder forward pass")
    parser.add_argument("--hnsw_index", action='store_true', help='If enabled, use inference time efficient HNSW index')
    parser.add_argument('--index_buffer', type=int, default=50000,
                            help="Temporal memory data buffer size (in samples) for indexer")

    if use_gpu:
        add_cuda_params(parser)
        args =parser.parse_args(['--model_file', model_name_or_path])
        setup_args_gpu(args)
        
    else:
        args =parser.parse_args(['--model_file', model_name_or_path])
        args.device = torch.device("cpu")
       
    return args

def query_by_embedding(query_text,doc_json,key:str="text",key_title:str=None,use_ctx_encoder:bool=True):    
    global DenseEncoder
    if not DenseEncoder:
        model_path=__DPR_LOCATION__
        pretrained_file=__PRETRAINED_MODEL_LOCATION__
        use_gpu=__USE_GPU__
        download_model()
        args=setup_args_from_checkpoint(model_path,use_gpu)
        DenseEncoder = DenseRetriever(args,pretrained_file,32,use_gpu)
    return DenseEncoder.compute_consine_score(query_text,doc_json,key,key_title,use_ctx_encoder)

def get_query_encodedvector(query_text):
    
    global DenseEncoder
    if not DenseEncoder:
        model_path=__DPR_LOCATION__
        pretrained_file=__PRETRAINED_MODEL_LOCATION__
        use_gpu=__USE_GPU__
        download_model()
        args=setup_args_from_checkpoint(model_path,use_gpu)
        DenseEncoder = DenseRetriever(args,pretrained_file,32,use_gpu)
        
    encoded_vector=DenseEncoder.generate_question_vectors([query_text]).data.numpy()[0]
        
    return encode_array(encoded_vector)