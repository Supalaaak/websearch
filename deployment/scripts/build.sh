#./build.sh <docker_name> <tag>

AWS_ACCOUNT_ID="911567254280"
AWS_REGION="ap-southeast-1"
DOCKER_NAME=${1}
GIT_TAG=${2}

echo "Building $DOCKER_NAME with tag $GIT_TAG"

CONTAINER_NAME_LATEST="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/$DOCKER_NAME:latest"
CONTAINER_NAME_TAG="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/$DOCKER_NAME:$GIT_TAG"

docker build -f Dockerfile.gpu -t $CONTAINER_NAME_LATEST -t $CONTAINER_NAME_TAG --network host .
docker push $CONTAINER_NAME_TAG
docker push $CONTAINER_NAME_LATEST