import tensorflow as tf

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
    # Memory growth must be set before GPUs have been initialized
        print(e)
    
import tensorflow_hub as hub
import tensorflow_text
from scipy import spatial
from typing import List, Tuple, Dict, Iterator
from config import __HUB_GPU__,elastic_faq
from knowledge_base.utils import encode_array
import requests
import json

SentenceEncoder = None

def compute_consine_vector(query_text,corpus_list: List[str]):    
    global SentenceEncoder
    if not SentenceEncoder:
        use_gpu=__HUB_GPU__
        if not(use_gpu):
            disable_gpu()
        SentenceEncoder = GoogleUSEEncoder()
    
    feature=SentenceEncoder.extract_embeddings([query_text])[0]
    centroids=SentenceEncoder.extract_embeddings([corpus_list])
    
    consine_vector=[]
    for j, centroid in enumerate(centroids):
        value=1-spatial.distance.cosine(feature, centroid)
        consine_vector.append(value)
    
    return consine_vector

def get_query_vector(query_text):
    global SentenceEncoder
    if not SentenceEncoder:
        use_gpu=__HUB_GPU__
        if not(use_gpu):
            disable_gpu()
        SentenceEncoder = GoogleUSEEncoder()
    
    feature=SentenceEncoder.extract_embeddings([query_text])
    return encode_array(feature[0])

def set_value(zip_output):
    faq_output,score_output=zip_output
    faq_output['score']=2*score_output["_score"]-1
    return faq_output

def query_faq_vectors(query_text,rescore_size=5):
    headers = {'Content-type': 'application/json'}
    url=elastic_faq
    try:
        query_vector=get_query_vector(query_text)
        rescore_query={
          "query" : {
                    "multi_match" : {
                        "query" : query_text,
                        "fields":["question"]
                    }
          },
          "rescore": {
            "window_size": rescore_size,
            "query": {
              "rescore_query": {
                  "function_score": {
                  "boost_mode": "replace",
                  "script_score": {
                    "script": {
                      "source": "binary_vector_score",
                      "lang": "knn",
                      "params": {
                        "cosine": True,
                        "field": "question_hub",
                        "encoded_vector": query_vector
                      }
                    }
                  }
                }
              },
              "query_weight": 0,
              "rescore_query_weight": 1
            }
          },"_source":[
              "id",
              "question","answer","plan_type"
           ],
        } 
        r = requests.post(url, data=json.dumps(rescore_query),headers=headers)
        elastic_outputs=json.loads(r.text)["hits"]["hits"]
        outputs=list(map(lambda x: x["_source"], elastic_outputs))
        mix_output = list(map(set_value, zip(outputs,elastic_outputs)))
        return mix_output
    except:
        return []
    
    return []

def disable_gpu():
    try:
        # Disable all GPUS
        tf.config.set_visible_devices([], 'GPU')
        visible_devices = tf.config.get_visible_devices()
        for device in visible_devices:
            assert device.device_type != 'GPU'
    except:
        # Invalid device or cannot modify virtual devices once initialized.
        pass

class GoogleUSEEncoder(object):
    """
    Does passage retrieving over the provided index and question encoder
    """
    def __init__(self, batch_size=100):
        embedding = "https://tfhub.dev/google/universal-sentence-encoder-multilingual-large/3"
        self.hub_layer = hub.KerasLayer(embedding, input_shape=[], 
                           dtype=tf.string, trainable=False)
        self.batch_size = batch_size

    def extract_embeddings(self, questions: List[str]):
        n = len(questions)
        bsz = self.batch_size
        query_vectors = []
        for j, batch_start in enumerate(range(0, n, bsz)):
            hub_corpus_embeddings=self.hub_layer(questions[batch_start:batch_start + bsz])
            query_vectors.extend(hub_corpus_embeddings)
        return query_vectors