
def transform_word(sentence):
    sentence=" "+sentence+" "
    sentence=sentence.replace("แพทย์แผนไทย","แพทย์ทางเลือก แพทย์แผนไทย")
    sentence=sentence.replace("แพทย์แผนจีน","แพทย์ทางเลือก แพทย์แผนจีน")
    sentence=sentence.replace("เลสิก","เลสิค")
    sentence=sentence.replace(" ER ","ER ฉุกเฉิน")
#     sentence=sentence.replace("ระยะเวลารอคอย"," ระยะเวลารอคอย waiting period ")
#     sentence=sentence.replace("ระยะรอคอย"," ระยะเวลารอคอย waiting period ")

    sentence=sentence.lower()

    sentence=sentence.replace("day case"," การผ่าตัดหรือหัตถการแบบผู้ป่วยนอก ")
    sentence=sentence.replace(" admit ","เข้ารับการรักษาในโรงพยาบาล")

    sentence=sentence.replace("เคลม","ได้คุ้มครอง")
    sentence=sentence.replace("ทำกิ๊ฟ","การแก้ไขปัญหาการมีบุตร ทำกิ๊ฟ")
    sentence=sentence.replace("trastuzumab","รักษาโรคมะเร็งรวมถึงเคมีบําบัด trastuzumab")
    sentence=sentence.replace("มุ่งเป้า","(Targeted therapy) ออกฤทธิ์จำเพาะเจาะจงต่อเซลล์มะเร็ง")
    sentence=sentence.replace(" ipd "," ผู้ป่วยใน ipd ")
    sentence=sentence.replace(" hiv "," เอดส์ hiv ")
    sentence=sentence.replace(" sle "," โรคเรื้อรัง sle ")

    if ("เคย" in sentence):
        sentence= sentence +" Pre-existing Condition"
    if ("คุ้มครอง" in sentence or "รอคอย" in sentence):
        sentence=sentence+ " waiting period "
    if ("กายภาพ" in sentence):
        sentence=sentence+ " กายภาพบำบัด "
    if ("ไม่นอนโรงพยาบาล" in sentence):
        sentence=sentence+ " ผู้ป่วยนอก OPD"
    if ("ผ่า" in sentence):
        sentence=sentence+ " หัตถการ "
    if ("f/u" in sentence or "follow up" in sentence or " opd" in sentence):
        sentence=sentence+ " ค่าธรรมเนียมปรึกษาแพทย์เวชปฏิบัติทั่วไปและแพทย์เฉพาะทาง OPD "
    
    sentence=sentence.strip()
    sentence=" ".join(sentence.split())
    
    return sentence


def transform_word2(sentence):
    sentence=" "+sentence+" "

    sentence=sentence.lower()

    sentence=sentence.replace("day case"," การผ่าตัดหรือหัตถการแบบผู้ป่วยนอก ")
    sentence=sentence.replace("trastuzumab","รักษาโรคมะเร็งรวมถึงเคมีบําบัด trastuzumab")
    sentence=sentence.replace("มุ่งเป้า","(Targeted therapy) ออกฤทธิ์จำเพาะเจาะจงต่อเซลล์มะเร็ง")


    if ("เคย" in sentence):
        sentence= sentence +" Pre-existing Condition"
    if ("คุ้มครอง" in sentence or "รอคอย" in sentence):
        sentence=sentence+ " waiting period "
    if ("กายภาพ" in sentence):
        sentence=sentence+ " กายภาพบำบัด "
    if ("ไม่นอนโรงพยาบาล" in sentence):
        sentence=sentence+ " OPD"
    if ("ผ่า" in sentence):
        sentence=sentence+ " หัตถการ "
    if ("f/u" in sentence):
        sentence=sentence+ " follow up"
    if ("เลสิก" in sentence or "เลสิค" in sentence or "lasik" in sentence):
        sentence=sentence+ " ทำเลสิค"
    sentence=sentence.strip()
    sentence=" ".join(sentence.split())
    
    return sentence

def transform_word3(sentence):

    sentence=sentence.lower()
    if ("trastuzumab" in sentence):
        sentence= sentence +" รักษาโรคมะเร็งรวมถึงเคมีบําบัด"
    if ("มุ่งเป้า" in sentence):
        sentence= sentence +" targeted therapy"
    if ("เคย" in sentence):
        sentence= sentence +" Pre-existing Condition"
#     if ("คุ้มครอง" in sentence or "รอคอย" in sentence):
#         sentence=sentence+ " waiting period"
    if ("แอดมิท"in sentence):
        sentence= sentence +" IPD"
    if ("กายภาพ" in sentence):
        sentence=sentence+ " กายภาพบำบัด"
    if ("ไม่นอนโรงพยาบาล" in sentence):
        sentence=sentence+ " OPD"
    if ("ไม่ได้นอน" in sentence):
        sentence=sentence+ " OPD"
    if ("ไม่นอน" in sentence):
        sentence=sentence+ " OPD"
    if ("f/u" in sentence):
        sentence=sentence+ " follow up"
    if ("เลสิก" in sentence or "เลสิค" in sentence or "lasik" in sentence):
        sentence=sentence+ " ทำเลสิค"
    sentence=sentence.strip()
    
    return sentence