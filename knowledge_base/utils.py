import os
import pathlib
from re import match
import pandas as pd
import base64
import numpy as np

from pandas.core.arrays import string_

PSYCHO_WORDS = []
with open('data/psycho_words.txt', 'r') as f:
    PSYCHO_WORDS = f.read().splitlines()

BOOST_WORDS = []
with open('data/boost_words.txt', 'r') as f:
    BOOST_WORDS = f.read().splitlines()

_syn_df = pd.read_csv('data/synonym.csv', header=None)
SYN_DICT = dict(zip(_syn_df[0], _syn_df[1]))


def psycho_check(sentence):
    for word in PSYCHO_WORDS:
        if word in sentence:
            new_sentence = sentence.replace(word, f' จิตเวช {word}')
            return new_sentence
        else:
            return sentence


def check_boost_words(sentence, url):
    for word in BOOST_WORDS:
        if word in sentence:
            new_url = f'{url}&defType=edismax&bq=text:({word})^10'
            return new_url
    return url


def translate(text, conversion_dict, before=None):
    """
    Translate words from a text using a conversion dictionary

    Arguments:
        text: the text to be translated
        conversion_dict: the conversion dictionary
        before: a function to transform the input
        (by default it will to a lowercase)
    """
    # if empty:
    if not text:
        return text
    # preliminary transformation:
    before = before or str.lower
    t = before(text)
    for key, value in conversion_dict.items():
        t = t.replace(key, value)
    return t


def get_plan_type(text):
    key_dict = {"แผน 1-4": "plan1, plan2, plan3, plan4", "แผน 1-3": "plan1, plan2, plan3",
                "แผน 1-2": "plan1, plan2", "แผน 2-4": "plan2, plan3, plan4", "แผน 2-3": "plan2, plan3",
                "แผน 3-4": "plan1, plan2", "แผน 1": "plan1", "แผน 2": "plan2", "แผน 3": "plan3", "แผน 4": "plan4",
                "plan 1": "plan1", "plan 2": "plan2", "plan 3": "plan3", "plan 4": "plan4",
                "แผน1": "plan1", "แผน2": "plan2", "แผน3": "plan3", "แผน4": "plan4"
                }
    plan_list = []
    clean_text = text.replace("elite", " ").replace("health", " ")
    for key in key_dict.keys():
        if(key in text):
            text = text.replace(key, key_dict[key])
            clean_text = clean_text.replace(key, " ")
            semi_list = key_dict[key].split(",")
            plan_list = plan_list+semi_list
    return clean_text, (list(set(list(plan_list))))


def isException(sentence):
    if("พื้นที่คุ้มครอง" in sentence):
        return False
    elif("พื้นที่ความคุ้มครอง" in sentence):
        return False
    elif( "จ่าย" in sentence):
        return True
    elif( "เบิก" in sentence):
        return True
    elif( "เคลม" in sentence):
        return True
    elif( "รอคอย" in sentence):
        return True
    elif( "คุ้มครอง" in sentence):
        return True
    elif( "เท่าไหร่" in sentence):
        return True
    elif( "เท่าไร" in sentence):
        return True
    elif( "สิทธิ์" in sentence):
        return True
    elif( "หรือเปล่า" in sentence):
        return True
    elif( "หรือไม่" in sentence):
        return True
    elif( "รับประกัน" in sentence):
        return True

dfloat32 = np.dtype('>f4')

def decode_float_list(base64_string):
    bytes = base64.b64decode(base64_string)
    return np.frombuffer(bytes, dtype=dfloat32).tolist()

def encode_array(arr):
    base64_str = base64.b64encode(np.array(arr).astype(dfloat32)).decode("utf-8")
    return base64_str