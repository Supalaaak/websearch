import argparse
from dpr.indexer.faiss_indexers import DenseFlatIndexer, DenseHNSWFlatIndexer
from dpr.utils.model_utils import get_model_obj, load_states_from_checkpoint, setup_for_distributed_mode
from dpr.models import init_biencoder_components
from dpr.options import add_cuda_params, add_encoder_params, add_tokenizer_params, set_encoder_params_from_state, setup_args_gpu
from .dense_retriever import DenseRetriever
import torch

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    add_encoder_params(parser)
    add_tokenizer_params(parser)
    add_cuda_params(parser)
    parser.add_argument('--match', type=str, default='string', choices=['regex', 'string'],
                        help="Answer matching logic type")
    parser.add_argument('--n-docs', type=int, default=200,
                        help="Amount of top docs to return")
    parser.add_argument('--validation_workers', type=int, default=16,
                        help="Number of parallel processes to validate results")
    parser.add_argument('--batch_size', type=int, default=2,
                        help="Batch size for question encoder forward pass")
    parser.add_argument('--index_buffer', type=int, default=50000,
                        help="Temporal memory data buffer size (in samples) for indexer")
    parser.add_argument("--hnsw_index", action='store_true',
                        help='If enabled, use inference time efficient HNSW index')
    args = parser.parse_args(
        ['--model_file', '/root/workspace/nlp3/DPR/output_model/dpr_biencoder.31.602'])
    setup_args_gpu(args)
    print(args)
    saved_state = load_states_from_checkpoint(args.model_file)
    set_encoder_params_from_state(saved_state.encoder_params, args)
    tensorizer, encoder, _ = init_biencoder_components(
        args.encoder_model_type, args, inference_only=True)
    encoder = encoder.question_model
    encoder, _ = setup_for_distributed_mode(
        encoder, None, args.device, args.n_gpu, args.local_rank, args.fp16)
    model_to_load = get_model_obj(encoder)
    prefix_len = len('ctx_model.')
    ctx_state = {key[prefix_len:]: value for (
        key, value) in saved_state.model_dict.items() if key.startswith('ctx_model.')}
    model_to_load.load_state_dict(ctx_state)

    prefix_len = len('question_model.')
    question_encoder_state = {key[prefix_len:]: value for (key, value) in saved_state.model_dict.items() if
                              key.startswith('question_model.')}
    model_to_load.load_state_dict(question_encoder_state)
    vector_size = 768
    if args.hnsw_index:
        index = DenseHNSWFlatIndexer(vector_size, args.index_buffer)
    else:
        index = DenseFlatIndexer(vector_size, args.index_buffer)

    retriever = DenseRetriever(encoder, args.batch_size, tensorizer, index)

    cos = torch.nn.CosineSimilarity(dim=1, eps=1e-6)
