import re
import deepcut


def extract_thaistring(test_str):
    regex = r'([\u0E00-\u0E7F]+)'
    matches = re.finditer(regex, test_str, re.MULTILINE | re.UNICODE)
    string_list, len_list = []
    for matchNum, match in enumerate(matches, start=1):
        word = match.group()
        string_list.append(word)
        len_list.append(len(word))
    return string_list, len_list


def check_thaistring(test_str):
    regex = r"([\u0E00-\u0E7F]+)"
    matches = re.finditer(regex, test_str, re.MULTILINE | re.UNICODE)
    string_list=[]
    len_list=[]
    for matchNum, match in enumerate(matches, start=1):
        word=match.group()
        string_list.append(word)
        len_list.append(len(word))
    if(len(len_list)>0):
        return True
    return False

def tokenize_thaisent(text):
    thai_sen = []
    all_sen = re.split(r'([\u0E00-\u0E7F]+)', text)
    for sen in all_sen:
        if check_thaistring(sen):
            thai_sen.append(' '.join(deepcut.tokenize(sen)))
        else:
            thai_sen.append(sen)
    return ''.join(thai_sen)

