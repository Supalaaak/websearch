import json
import requests
import deepcut
from .utils import *
from .transform import *
from .lang_utils import *

from dpr.generator.util import get_query_encodedvector

from config import __SOLRHOST__, elastic_kb

HOST = __SOLRHOST__

def query_disease_json(question):
    url = f'{HOST}/solr/disease_table/select?fq=ipd:1&fl=id,score,text,wp,Group,daycase&q=(text:({question}) or Group:({question}))'
    r = requests.get(url)
    try:
        output = json.loads(r.text)['response']['docs']
        return output
    except:
        return []

def faq_base_json(question):
    question = translate(question.lower(), SYN_DICT)
    url=f'{HOST}/solr/elite_qa2/select?fl=id,score,plan_type,policy_type,question,answer&q=question:({question})'
    outputs=[]
    try:
        r = requests.get(url)
        outputs=json.loads(r.text)['response']['docs']
    except:
        return []
    
    return outputs

def faq_kb_json(question):
    question = transform_word2(question)
    question = translate(question.lower(), SYN_DICT)
    question = psycho_check(question)

    clean_text, plan_list = get_plan_type(question.lower())
    token_text = tokenize_thaisent(clean_text)
    url = f'{HOST}/solr/faq_kb/select?rows=10&fq=-policy_type:definition&fl=id,case,policy_type,text,plan1,plan2,plan3,plan4,url,score&q=(case:({token_text}) or token_text:({token_text}))'
    try:
        url = check_boost_words(question, url)
        r = requests.get(url)
        output = json.loads(r.text)['response']['docs']
        d = {r['text']: r for r in output}.values()
        return list(d), question
    except:
        return [],question


def knowledge_base_json(question):
    question = transform_word(question)
    question = translate(question.lower(), SYN_DICT)
    question = psycho_check(question)
    clean_text, plan_list = get_plan_type(question.lower())
    if len(plan_list) <= 0:
        token_question = ' '.join(deepcut.tokenize(clean_text.strip()))
        if isException(question):
            url = f'{HOST}/solr/elite_plan2/select?rows=50&fq=-policy_type:definition&fq=-policy_type:plan1_benefit_1_ipd_plus&fq=-policy_type:plan2_benefit_1_ipd_plus&fq=-policy_type:plan3_benefit_1_ipd_plus&fq=-policy_type:plan4_benefit_1_ipd_plus&fl=id,score,plan_type,policy_type,text,score&q=token_text:({token_question})'
        else:
            url = f'{HOST}/solr/elite_plan2/select?rows=50&fq=-policy_type:plan1_benefit_1_ipd_plus&fq=-policy_type:plan2_benefit_1_ipd_plus&fq=-policy_type:plan3_benefit_1_ipd_plus&fq=-policy_type:plan4_benefit_1_ipd_plus&fl=id,score,plan_type,policy_type,text,score&q=token_text:({token_question})'
    else:
        clean_text = ' '.join(deepcut.tokenize(clean_text.strip()))
        if isException(question):
            url = f'{HOST}/solr/elite_plan2/select?rows=50&fq=-policy_type:definition&fq=-policy_type:plan1_benefit_1_ipd_plus&fq=-policy_type:plan2_benefit_1_ipd_plus&fq=-policy_type:plan3_benefit_1_ipd_plus&fq=-policy_type:plan4_benefit_1_ipd_plus&fq=plan_type:({plan_list[0]} or all)&fq=-policy_type: definition&fl=id,score,plan_type,policy_type,text,score&q=token_text:({clean_text})'
        else:
            url = f'{HOST}/solr/elite_plan2/select?rows=50&fq=-policy_type:plan1_benefit_1_ipd_plus&fq=-policy_type:plan2_benefit_1_ipd_plus&fq=-policy_type:plan3_benefit_1_ipd_plus&fq=-policy_type:plan4_benefit_1_ipd_plus&fq=plan_type:({plan_list[0]} or all)&fl=id,score,plan_type,policy_type,text,score&q=token_text:({clean_text})'
    url = check_boost_words(question, url)
    r = requests.get(url)
    output = json.loads(r.text)['response']['docs']
    d = {r['text']: r for r in output}.values()

    return list(d), question


def knowledge_base_json2(question):
    question = transform_word(question)
    question = translate(question.lower(), SYN_DICT)
    question = psycho_check(question)
    clean_text, plan_list = get_plan_type(question.lower())
    if len(plan_list) <= 0:
        token_question = ' '.join(deepcut.tokenize(clean_text.strip()))
        if isException(question):
            url = f'{HOST}/solr/elite_plan2/select?rows=25&fq=-policy_type:definition&fl=id,score,plan_type,policy_type,text,score&q=token_text:({token_question})'
        else:
            url = f'{HOST}/solr/elite_plan2/select?rows=25&fl=id,score,plan_type,policy_type,text,score&q=token_text:({token_question})'
    else:
        clean_text = ' '.join(deepcut.tokenize(clean_text.strip()))
        if isException(question):
            url = f'{HOST}/solr/elite_plan2/select?rows=25&fq=-policy_type:definition&fq=plan_type:({plan_list[0]} or all)&fq=-policy_type: definition&fl=id,score,plan_type,policy_type,text,score&q=token_text:({clean_text})'
        else:
            url = f'{HOST}/solr/elite_plan2/select?rows=25&fq=plan_type:({plan_list[0]} or all)&fl=id,score,plan_type,policy_type,text,score&q=token_text:({clean_text})'
    
    url = check_boost_words(question, url)
    r = requests.get(url)
    output = json.loads(r.text)['response']['docs']
    d = {r['text']: r for r in output}.values()

    return list(d), question

def get_elastickb_json(query_text,vector_weight=2000,min_kb_score=0):
    elastic_url=elastic_kb
    query_text=query_text.replace("ใส้","ไส้")
    query_vector=get_query_encodedvector(query_text)
    tran_text=transform_word3(query_text)
    rescore_query={
      "size":10,
      "query" : {
                "multi_match" : {
                "query" : tran_text,
                "fields":["text","case"],
                "type":"most_fields"
                }
      },
      "rescore": {
        "window_size": 50,
        "query": {
          "rescore_query": {
              "function_score": {
              "boost_mode": "replace",
              "script_score": {
                "script": {
                  "source": "binary_vector_score",
                  "lang": "knn",
                  "params": {
                    "cosine": True,
                    "field": "dpr_embedding_vector",
                    "encoded_vector": query_vector
                  }
                }
              }
            }
          },
          "query_weight": 0,
          "rescore_query_weight": 1
        }
      },"_source":[
          "id",
          "text","score","case","plan1","plan2","plan3","url","policy_type"
       ],
    }
#     rescore_query={
#       "min_score": min_kb_score,
#       "query": {
#         "function_score": {
#           "boost_mode": "replace",
#           "script_score": {
#             "script": {
#               "source": "binary_vector_score",
#               "lang": "knn",
#               "params": {
#                 "cosine": True,
#                 "field": "dpr_embedding_vector",
#                 "encoded_vector":query_vector
#               }
#             }
#           }
#         }
#       },
#        "rescore" : [ {
#           "window_size" : 10,
#           "query" : {
#              "rescore_query" : {
#                 "multi_match" : {
#                     "query" : tran_text,
#                     "fields":["text","case"],
#                     "type":"most_fields"
#                 }
#              },
#              "query_weight" : vector_weight,
#              "rescore_query_weight" : 1
#           }
#        },
#      ],"_source":[
#           "id",
#           "text","score","case","plan1","plan2","plan3","url","policy_type"
#        ],
#     }
    headers = {'Content-type': 'application/json'}
    r = requests.post(elastic_url, data=json.dumps(rescore_query),headers=headers)
    elastic_outputs=json.loads(r.text)["hits"]["hits"]
    outputs=list(map(lambda x: x["_source"], elastic_outputs))
    
    def set_value(zip_output):
        kb_output,score_output=zip_output
        kb_output['score']=score_output["_score"]
        return kb_output
    
    mix_output = list(map(set_value, zip(outputs,elastic_outputs)))

    return mix_output