import copy
from datetime import datetime
from KnowledgeBaseQuery import rerank_DPR, rerank_GoogleUSE, rerank_disease_table_faq_DPR, rerank_faq_DPR,get_faq_DPR, get_DPR
from knowledge_base.query import faq_base_json, faq_kb_json
from flask import Flask, request, jsonify, render_template
from urllib import parse

app = Flask(__name__)


@app.route('/query_api', methods=['GET', 'POST'])
def query_api():
    if request.method == 'POST':
        query = request.form['query']
    else:
        query = request.args.get('query', None)
    
#     data, question = faq_kb_json(query)
#     data = data[:10]
#     mix_output = rerank_DPR(query, copy.deepcopy(data))
    mix_output=get_DPR(query)
    return jsonify(mix_output)

#knowledge api
@app.route('/kb_api', methods=['GET', 'POST'])
def kb_api():
    if request.method == 'POST':
        query = request.form['query']
    else:
        query = request.args.get('query', None)
    
#     data, question = faq_kb_json(query)
#     data = data[: 10]
#     faq_kb_outputs =  rerank_faq_DPR(query, copy.deepcopy(data))
    faq_kb_outputs =  get_faq_DPR(query, min_kb_score=0.8)

    return jsonify(faq_kb_outputs)

@app.route('/GUse_dpr', methods=['GET', 'POST'])
def compare_use_dpr():
    start_time = datetime.now()
    
    if request.method == 'POST':
        query = request.form['query']
        start = request.form['start']
        hits = request.form["hits"]
    else:
        query = request.args.get('query', None)
        start = request.args.get('start', 0, type=int)
        hits = request.args.get('hits', 10, type=int)
        
    if start < 0 or hits < 0:
        return 'Error, start or hits can not be negative numbers'
    data = []
    
    if query:
        data = faq_base_json(query)
        use_outputs = rerank_GoogleUSE(query, copy.deepcopy(data))
        dpr_outputs = rerank_DPR(query, copy.deepcopy(
            data), key='question', use_ctx_encoder=False)
        for doc in use_outputs:
            doc['url'] = doc['id']
            doc['title'] = doc['question']
            doc['description'] = doc['answer']
        for doc in dpr_outputs:
            doc['url'] = doc['id']
            doc['title'] = doc['question']
            doc['description'] = doc['answer']

        results = {'result1': use_outputs, 'result2': dpr_outputs}
        i= int(start/hits)
        total= len(data)
        maxi= i + int(total/hits)
        range_pages= range(i - 5, i + 5 if i + 5 < maxi else maxi) if i >= 0 else range(0, maxi if maxi < 10 else 10)
        return render_template('spatial/FAQindex.html', query=query, response_time=(datetime.now() - start_time).total_seconds(), total=total, hits=hits, start=start, range_pages=range_pages, results=results, page=i, maxpage=maxi-1)
    return render_template('spatial/FAQindex.html')

@ app.route('/', methods=['GET', 'POST'])
def search():
    start_time = datetime.now()

    if request.method == 'POST':
        query = request.form['query']
        start=request.form['start']
        hits = request.form["hits"]
    else:
        query = request.args.get("query", None)
        start = request.args.get("start", 0, type=int)
        hits = request.args.get("hits", 10, type=int)
    
    if start < 0 or hits < 0:
        return "Error, start or hits cannot be negative numbers"
    data = []
    if query:
#         data, question = faq_kb_json(query)
#         data = data[: 10]
#         faq_kb_outputs =  rerank_faq_DPR(query, copy.deepcopy(data))
        faq_kb_outputs=get_faq_DPR(query)
    
        all_outputs=[]
        if(faq_kb_outputs.get("faq",-1)!=-1):
            if(len(faq_kb_outputs["faq"])>0):
                doc=faq_kb_outputs["faq"][0]
                if(doc["score"]>=0.75):
                    doc["url"]= "source: FAQ"
                    doc["title"] = doc["question"]
                    doc["description"]= doc["answer"]
                    doc["score"]= 'score {:.2%}'.format(doc["score"])
                    all_outputs=[doc]
            
        if(faq_kb_outputs.get("kb",-1)!=-1):
            for doc in faq_kb_outputs["kb"]:
                doc["url"]= "source: Knowledge base"
                doc["title"]= doc["case"]
                doc["description"]= doc["text"]
                doc["score"] = 'score {:.2%}'.format(doc["score"])
                all_outputs.append(doc)
                
        results = all_outputs
        i = int(start/hits)
        total= len(data)
        maxi = 1+int(total/hits)
        range_pages = range(i-5, i+5 if i+5 < maxi else maxi) if i >= 6 else range(0, maxi if maxi < 10 else 10)
        return render_template('spatial/index.html', query=query, response_time=(datetime.now() - start_time).total_seconds(), total=total, hits=hits, start=start, range_pages=range_pages, results=results, page=i, maxpage=maxi-1)
    return render_template('spatial/index.html')


@ app.route('/ab_testing', methods=['GET', 'POST'])
def search_ab_tesing():
    start_time = datetime.now()

    if request.method == 'POST':
        query = request.form['query']
        start=request.form['start']
        hits = request.form["hits"]
    else:
        query = request.args.get("query", None)
        start = request.args.get("start", 0, type=int)
        hits = request.args.get("hits", 10, type=int)
    
    if start < 0 or hits < 0:
        return "Error, start or hits cannot be negative numbers"
    data = []
    if query:
        data, question = faq_kb_json(query)
        data = data[: 10]
        mix_output = rerank_disease_table_faq_DPR(query, copy.deepcopy(data))
        for doc in data:
            doc["url"]= doc["id"]
            if(doc.get("title", -1) == -1):
                doc["title"] = doc["id"]  # answer
            doc["description"]= doc["text"]
            doc["score"]= 'score {:.2%}'.format(doc["score"])

        for doc in mix_output:
            doc["url"]= doc["id"]
            if(doc.get("title", -1) == -1):
                doc["title"]= doc["id"]
            doc["description"]= doc["text"]
            doc["score"] = 'score {:.2%}'.format(doc["score"])
            
        results = {"result1": data[: 10], "result2": mix_output[: 10]}
        i = int(start/hits)
        total= len(data)
        maxi = 1+int(total/hits)
        range_pages = range(i-5, i+5 if i+5 < maxi else maxi) if i >= 6 else range(0, maxi if maxi < 10 else 10)
        return render_template('spatial/index-ab.html', query=query, response_time=(datetime.now() - start_time).total_seconds(), total=total, hits=hits, start=start, range_pages=range_pages, results=results, page=i, maxpage=maxi-1)
    return render_template('spatial/index-ab.html')


# -- JINJA CUSTOM FILTERS -- #

@app.template_filter('truncate_title')
def truncate_title(title):
    """
    Truncate title to fit in result format.
    """
    return title if len(title) <= 70 else title[:70]+"..."

@app.template_filter('truncate_description')
def truncate_description(description):
    """
    Truncate description to fit in result format.
    """
    if len(description) <= 160 :
        return description

    cut_desc = ""
    character_counter = 0
    for i, letter in enumerate(description) :
        character_counter += 1
        if character_counter > 160 :
            if letter == ' ' :
                return cut_desc+"..."
            else :
                return cut_desc.rsplit(' ',1)[0]+"..."
        cut_desc += description[i]
    return cut_desc

@app.template_filter('truncate_url')
def truncate_url(url):
    """
    Truncate url to fit in result format.
    """
    url = parse.unquote(url)
    if len(url) <= 60 :
        return url
    url = url[:-1] if url.endswith("/") else url
    url = url.split("//",1)[1].split("/")
    url = "%s/.../%s"%(url[0],url[-1])
    return url[:60]+"..." if len(url) > 60 else url
