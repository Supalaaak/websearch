FROM python:3.7-slim-buster

RUN apt update && apt install -y apt-utils build-essential ca-certificates libssl-dev

ENV FLASK_APP app.py
WORKDIR /workspace
COPY . .
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install git+https://github.com/huggingface/transformers.git@v3.0.2

#RUN python -c 'from dpr.generator.util import download_model; download_model()'

EXPOSE 8000/tcp

CMD [ "flask", "run", "--host=0.0.0.0" , "--port=8000"]

# build example : docker build -t websearch .
# run example : docker run -p 80:8000 websearch
# recommended memory limit> 8g
