import deepcut
import argparse
import os
import csv
import glob
import json
import gzip
import logging
import pickle
import pathlib
import pandas as pd
import time
from typing import List, Tuple, Dict, Iterator
from scipy import spatial

import numpy as np

from knowledge_base.query import faq_base_json,query_disease_json,get_elastickb_json
from knowledge_base.utils import get_plan_type
from GoogleUSEquery import compute_consine_vector,query_faq_vectors

import dpr.generator.util as retriever
import urllib.parse

import requests
import re

def rerank_DPR(quest,outputs,key='text',use_ctx_encoder=True):
    if(len(outputs)>0):
        mix_output = retriever.query_by_embedding(quest,outputs,key=key,use_ctx_encoder=use_ctx_encoder)
        return mix_output
    return []

def rerank_GoogleUSE(quest,faq_output):
    if(len(faq_output)>0):
        question_list = list(map(lambda x : x['question'], faq_output))
        cosine_vector=compute_consine_vector(quest,question_list)
        def set_value(zip_output):
            faq_output,score=zip_output
            faq_output['score']=score
            return faq_output
        mix_output = list(map(set_value, zip(faq_output,cosine_vector)))
        mix_output=sorted(mix_output, key=lambda x: x['score'], reverse=True)
        return mix_output
    return []

def rerank_disease_table_faq_DPR(quest,outputs):
    if(len(outputs)>0):
        #query faq
        clean_quest,plan_list= get_plan_type(quest.lower())   

        #query faq    
#         faq_outputs=faq_base_json(quest)
        faq_outputs=query_faq_vectors(quest)
        faq_answer=[]
        if(len(faq_outputs)>0):
            faq_outputs = retriever.query_by_embedding(clean_quest,faq_outputs,key='question',key_title=None, use_ctx_encoder=False)
            faq_outputs[0]["title"]=faq_outputs[0]["question"]
            faq_outputs[0]["text"]=faq_outputs[0]["answer"]
            faq_answer=[faq_outputs[0]]

        #query policy_plan based on dpr model's ranking score 
        mix_output=retriever.query_by_embedding(clean_quest,outputs,key="text",key_title="case")

        #query disease table based on dpr model's ranking score 
        dc_output=query_disease_json(clean_quest)
        if(len(dc_output)>0):
            dc_output=retriever.query_by_embedding(clean_quest,dc_output,key='text',key_title=None,use_ctx_encoder=False)

            doc=dc_output[0]
            if(doc["text"]!=doc["Group"]):
                    text= "{} {}".format(doc["Group"],doc["text"])
            else:
                text=doc["text"]

            value=doc['score']
            if(value>0.5):
                new_text='''{} มีระยะเวลารอคอย {} วัน
                    - กรณีเข้ารับการรักษาเป็นผู้ป่วยในของโรงพยาบาล หรือ IPD คุ้มครอง ทุกแผน
                    - ผลประโยชน์ค่ารักษาพยาบาลผู้ป่วยนอก หรือ OPD คุ้มครอง เฉพาะ แผน 2-4'''.format(text,doc["wp"])
                new_dc={"id":doc["id"],"score":value,"text":new_text,"plan_type":"all","policy_type":"benefit_1_ipd"}

                mix_output=[new_dc]+mix_output
                mix_output=sorted(mix_output, key=lambda x: x['score'], reverse=True)

        return faq_answer+mix_output
    return []

#old bot api
def rerank_faq_DPR(quest,outputs):
    if(len(outputs)>0):
        #query faq
        clean_quest,plan_list= get_plan_type(quest.lower())   

        #query faq    
#         faq_outputs=faq_base_json(quest)
#         faq_answer=[]
#         if(len(faq_outputs)>0):
#             faq_answer =rerank_GoogleUSE(quest,faq_outputs)
        faq_answer=query_faq_vectors(quest)
        #query policy_plan based on dpr model's ranking score 
        mix_output=retriever.query_by_embedding(clean_quest,outputs,key="text",key_title="case")

        return {"faq":faq_answer,"kb":mix_output,"plan":plan_list}
    else:
        #query faq   
        clean_quest,plan_list= get_plan_type(quest.lower())
        faq_outputs=faq_base_json(quest)
        faq_answer=[]
        if(len(faq_outputs)>0):
            faq_answer =rerank_GoogleUSE(quest,faq_outputs)
            return {"faq":faq_answer,"kb":[],"plan":plan_list}
        
    return {"faq":[],"kb":[],"plan":[]}

def get_faq_DPR(quest,min_kb_score=0):

    clean_quest,plan_list= get_plan_type(quest.lower())   
    faq_answer=query_faq_vectors(quest)
    
    try:
        mix_output=get_elastickb_json(clean_quest,min_kb_score=min_kb_score)
        return {"faq":faq_answer,"kb":mix_output,"plan":plan_list}
    except:
        return {"faq":faq_answer,"kb":[],"plan":plan_list}
        
    return {"faq":[],"kb":[],"plan":[]}

def get_DPR(quest,min_kb_score=0):

    clean_quest,plan_list= get_plan_type(quest.lower())   
    
    try:
        mix_output=get_elastickb_json(clean_quest,min_kb_score=min_kb_score)
        return mix_output
    except:
        return []
        
    return []