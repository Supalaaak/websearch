__PRETRAINED_MODEL_SERVER__="https://sagemaker-aigen-models.s3.ap-southeast-1.amazonaws.com/ainlp/transformers/xlmr.base.zip?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA5IPNEOMEC6RLLT4U%2F20210127%2Fap-southeast-1%2Fs3%2Faws4_request&X-Amz-Date=20210127T024040Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=20b917d53f26cc78a70af479ed034959e634528848fa3a620507e375d8a183ce"
# __PRETRAINED_MODEL_LOCATION__="model/xlmr.base"
__PRETRAINED_MODEL_LOCATION__="/root/workspace/nlp3/DPR/xlmr.base"
__DPR_MODEL_SERVER__="https://sagemaker-aigen-models.s3.ap-southeast-1.amazonaws.com/ainlp/dpr_model/dpr_biencoder.31.602.cp?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA5IPNEOMEC6RLLT4U%2F20210127%2Fap-southeast-1%2Fs3%2Faws4_request&X-Amz-Date=20210127T052357Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=76a13e29add591d13b2cd6a48aedc232d729668e56ec524a735f8836957ac2f6"
# __DPR_LOCATION__="model/dpr_biencoder.31.602.cp"
# __DPR_LOCATION__="/root/workspace/nlp3/DPR/output_model/dpr_biencoder.36.504"
__DPR_LOCATION__="/root/workspace/nlp3/DPR/checkpoint/dpr_biencoder.36.504.cp"
# __DPR_LOCATION__="/root/workspace/nlp3/DPR/checkpoint/dpr_biencoder.31.364.cp"
# __KBHOST__="https://0ce261cf-nlp3-8983.container.space.aigen.dev"
__USE_GPU__=False
__HUB_GPU__=False
__BATCH_SIZE__=32
__SOLRHOST__ = 'https://0ce261cf-nlp3-8983.container.space.aigen.dev'
elastic_kb="http://192.168.10.172:9200/faq_kb/_search"
elastic_faq="http://192.168.10.172:9200/elite_qa2/_search"