# Knowledge base api

api of knowledge base with a simple web search engine https://Supalaaak@bitbucket.org/Supalaaak/websearch.git

## DEMO
A demo can be found here : http://localhost:port

## INSTALL AND RUN

### REQUIREMENTS
This tool requires *Python3.7* and the web search engine API (see link above).

### WITH PIP
```
git https://Supalaaak@bitbucket.org/Supalaaak/websearch.git
cd websearch
pip install -r requirements.txt
```

Then, run the tool :
```
FLASK_APP=app.py flask run --port 80
```

To run in debug mode, prepend `FLASK_DEBUG=1` to the command :
```
FLASK_DEBUG=1 ... flask run --port 80
```

### WITH DOCKER
To run the tool with Docker, you have build yourself a Docker image or a GPU Docker image:
```
git clone https://Supalaaak@bitbucket.org/Supalaaak/websearch.git
cd websearch
docker build -t websearch . or docker build -t websearch -f Dockerfile.gpu .
```


## USAGE AND EXAMPLES
To use the search engine, just type this endpoint in your web browser : http://localhost/

![Web search engine](images/kb_search_ui.png?raw=true "Search Engine" )

Warning: long execution for the first query (load models)
```
http://localhost:port/query_api?query=เอดส์รับประกันไหม
```

## LICENCE
MIT
